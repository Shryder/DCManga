var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var blogSchema = new Schema({
    id:  Number,
    title: String,
    content: String,
    date: String,
    likes: Array
}, { collection: 'blog' });

blogSchema.plugin(autoIncrement.plugin, {model: 'blog', field: 'id', startAt: 1});

module.exports = mongoose.model('blog', blogSchema);
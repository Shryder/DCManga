var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var chapterSchema = new Schema({
    id:  Number,
    title: String,
    uploader: String,
    views: Number,
    date: String,
    pages: Array
}, { collection: 'chapters' });

chapterSchema.plugin(autoIncrement.plugin, {model: 'chapters', field: 'id', startAt: 1});

chapterSchema.statics.getChapter = function(id, cb){
	this.findOne({id: id}, function(err, data){
		cb(data);
	});
}

module.exports = mongoose.model('chapters', chapterSchema);
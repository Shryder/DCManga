var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var commentSchema = new Schema({
    id:  Number,
    chapter: Number, 
    userid: Number,
    content: String,
    quote: Number,
    time: Date
}, { collection: 'comments' });

commentSchema.plugin(autoIncrement.plugin, {model: 'comments', field: 'id', startAt: 1});

module.exports = mongoose.model('comments', commentSchema);
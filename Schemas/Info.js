var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var infoSchema = new Schema({
    totalViews: Number
}, { collection: 'Info' });

infoSchema.statics.getTotalViews = function(cb){
	this.findOne({}, function(err, data){
		if(err) throw err;
		cb(data.totalViews);
	});
}

infoSchema.statics.incTotalViews = function(){
	this.findOneAndUpdate({}, { $inc: { totalViews: 1 }}).exec();
}


module.exports = mongoose.model('info', infoSchema);
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var loginSchema = new Schema({
    uid:  Number,
    clientInfo: Schema.Types.Mixed,
    createdAt: Date,
    token: String,
    expiresAt: Date
}, { collection: 'logins' });

module.exports = mongoose.model('logins', loginSchema);
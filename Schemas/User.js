var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var userSchema = new Schema({
    id: Number,
    username: String,
    avatar: String,
    email: String,
    password: String,
    registerDate: Number,
    rank: Number,
    banned: Boolean, 
    lastSeen: Date
}, { collection: 'users' });

userSchema.plugin(autoIncrement.plugin, {model: 'users', field: 'id', startAt: 1});

userSchema.statics.getInfo = function (uid, cb){
	this.findOne({id: uid}, function(err, data){
		if(err) throw err;
		return cb(data);
	});
}

userSchema.statics.updateUser = function(id){
	this.findOneAndUpdate({id: id }, { $set: { lastSeen: new Date() } }, function(err, data){
        if(err) throw err;
    });

}

module.exports = mongoose.model('users', userSchema);
function checkAuth(req, res, next) {
  if (!req.session.user) {
    res.status(404);
  } else {
    next();
  }
}

module.exports = checkAuth;
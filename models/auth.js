var User = require('../Schemas/User');
var bcrypt = require('bcrypt-nodejs');

exports.login = function(username, password, cb){
	User.findOne({username: username}, function(err, result){
		if(err) throw err;
		if(result !== null){
			if(bcrypt.compareSync(password, result.password)){
				if(result.rank == 0){ 
					//Banned
					cb(2, result);
				}else{
					//Success
					cb(1, result);
				}
			}else{
				cb(0, null);
			}
		}else{
			cb(0, null);
		}
	});
}

exports.register = function(username, email, password, cb){
	User.findOne({ $or: [{username: username}, {email: email}] }, function(err, result){
		if(result === null){
			new User({
				username: username,
				email: email,
				password: bcrypt.hashSync(password),
				registerDate: Date.now(),
				rank: 1,
				banned: false,
				lastSeen: null
			}).save(function(err, data){
				if(err) {
					cb("fail");
				}else{
					cb("success");	
				}
			});
		}else if(result.username === username) {
			cb("used username");
		}else if(result.email === email){
			cb("used email");
		}else{
			cb("fail");
		}
	});
}
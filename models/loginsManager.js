var Login = require('../Schemas/Logins.js')
	, randomstring = require("randomstring")
	, User = require('../Schemas/User.js');

// Will be working on a better "login manager" thing sometime soon.
// I probably don't even have to do this thing in a seperate collection
// No idea mane

exports.setupToken = function(uid, clientInfo, cb){
	var retToken = randomstring.generate(25) + Date.now();
	new Login({uid: uid, clientInfo: clientInfo, token: retToken, createdAt: Date.now(), expiresAt: Date.now() + 12096e5}).save(function(err, data){
		if(err) throw err;	
		cb(retToken);
	});
}

exports.getUser = function(token, cb){
	Login.findOne({token: token}, function(err, data){
		if(err) throw err;
		if(!data){
			cb(false, null);
		}else{
			User.getInfo(data.uid, function(data){
				cb(true, data);	
			});
		}
	});
}

exports.destroy = function(param){
	Login.findOne({ $or : [ { uid: param }, { token: param} ]}).remove().exec();
}

exports.getToken = function(user, cb){
	Login.findOne({uid: user}, function(err, data){
		if(err) throw err;
		if(!data){
			cb(false, null);
		}else{
			User.getInfo(data.uid, function(data){
				cb(true, data);	
			});
		}
	});
}
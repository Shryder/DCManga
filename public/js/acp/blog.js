$(document).ready(function(){
	var cursorPosStart, cursorPosEnd;
	function insertLink(){
		var v = $(".blogcontent").val();
		var textBefore = v.substring(0, cursorPosStart );
		var textAfter  = v.substring(cursorPosEnd, v.length );
		var paste = "<a href='" + $(".linkURL").val() +"'>"+ $(".linkName").val() +"</a>";

		$('.blogcontent').val( textBefore + paste + textAfter );
		$(this).dialog("close");
		$(this).find("form")[0].reset();
	}

	var dialog = $("#dialog-form").dialog({
      autoOpen: false,
      height: 250,
      width: 350,
      modal: true,
      buttons: {
      	"Done": insertLink
      }
    });
    $(".tools .insertlink").click(function(e){
    	e.preventDefault();

    	cursorPosStart = $('.blogcontent').prop('selectionStart');
		cursorPosEnd = $('.blogcontent').prop('selectionEnd');

    	dialog.dialog("open");
    });
});
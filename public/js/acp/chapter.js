$(document).ready(function(){
	function loadPages(){
		$(".pages").html("<img src='/img/loading.svg' style='width:300px;' class='loading'/>");

		$.ajax({
			url: '/api/get/chapter',
			type:'POST',
			data: {id: chapterId},
		}).done(function(data){
			if(data.status === "fail"){
				error(data.message);
				$(".container").html("");
				return;
			}

			$(".pages").html("");

			$.each(data.chapter.pages, function(index, value){
				$(".pages").append("<li class='page box' page='"+value+"'><img class='pageView' src='/ch/" + data.chapter.id + "/" + value + "'/><form method='POST' class='pageForm' enctype='multipart/form-data'><button class='btn btn-primary uploadBtn'>Replace </button><button style='float:right;' class='btn btn-primary deleteBtn' href='#'>Delete </button><input type='file' name='page' class='actualUploadBtn'/><input type='hidden' name='chapterId' value='" + data.chapter.id + "'/><input type='hidden' name='pageId' value='" + value + "'/></form></li>");
				$(".newname").val(data.chapter.title);
			})
			$(".refresh").text("Refresh pages (" + data.chapter.pages.length + ")" ); 

			$(".pages").append("<li><div class='add box'><span class='glyphicon glyphicon-plus' area-hidden='true'></span><form method='POST' class='addForm' enctype='multipart/form-data'><input type='hidden' name='chapterId' value='" + chapterId + "' /><input type='file' style='display:none;' class='addFileInput' name='page'/></form></div></li>");
		});
	}

	loadPages();
	var progress = $(".progress").progressbar(
		{
			value: 0,
			create: function(event, ui) {
				$(this).find('.ui-widget-header').css({'background-color':'#337ab7'})
			}
		});

	$(".zipUpload").click(function(){
		$(".archiveUpload").trigger("click");
	});

	$(".archiveUpload").on("change", function(){
		$(this).closest("form").submit();
	});

	$('body').on("submit", ".archiveForm", function(e){
		e.preventDefault();
		if(!$(this).find(".archiveUpload").val()) return;
		var image = $(this).siblings('.pageView');
		$(".progress").show();
		$.ajax({
			url: "/acp/chapter/bulk",
			type: 'POST',

			// Form data
			data: new FormData($(this)[0]),
			cache: false,
			contentType: false,
			processData: false,
			progress: function(e) {
				if(e.lengthComputable) {
					var pct = (e.loaded / e.total) * 100;
					progress.progressbar("value", pct);
				}else {
					console.warn('Content Length not reported!');
				}
			}}).done(function(data){
			if(data.status === "success"){
				$.notify("Successfully uploaded ZIP file.", "success");
				$(".progress").hide();
				loadPages();
			}else{
				$.notify("Something went wrong.", "error");
			}
		});

	});

	$('.editName').click(function(e){
		e.preventDefault();
		if($(".newname").is(":visible")){
			$.post("/acp/chapter/name", {id: chapterId, newName: $(".newname").val()}, function(data){
				if(data.status === "success"){
					$.notify("Successfully changed chapter name.", "success");
				}else{
					$.notify("There was an error while trying to change chapter name.", "error");
				}
			});
		}else{
			$(".newname").show("slow");
		}
	});

    $(".pages").sortable({ items: ".page", update: function(e, ui){
    	$('.saveOrder').show();
    }});

    $(".saveOrder").click(function(){
    	var newOrder = [];
    	var children = $(".pages").children(".page");

    	children.each(function(i){
    		newOrder.push($(children[i]).attr('page'));
    	});

    	$.post('/acp/chapter/order', { id: chapterId, newOrder: newOrder }, function(data){
    		if(data.status === "success"){
    			$.notify("Successfully saved the new pages order.", "success");
    			loadPages();
    		}else{
    			$.notify("Something went wrong.", "error");
    		}
    	});
    });

	$("body").on("click", ".uploadBtn", function(){
		$(this).siblings(".actualUploadBtn").trigger("click");
	})
	$("body").on('click', ".refresh", function(){
		loadPages();
	});

	$("body").on("submit", "#deleteChapter", function(e){
		e.preventDefault();
		$.post($(this).attr('action'), $(this).serialize(), function(data){
			if(data.status === "success"){
				$(".pages").html("");
				success("Successfully deleted this chapter. <a href='/acp/chapters/>Back</a>");
				$('.controls').html('');
			}else{
				if(data.status === "fail" && data.message) error(data.message);
				else error("Something went wrong while trying to delete chapter.");
				
			}
		});

	});

	$("body").on('click', ".deleteBtn" ,function(){
		var page = $(this).closest(".page").attr('page');

		$.ajax({
			url: "/acp/chapter/page/delete",
			type: 'POST',

			// Form data
			data: {id: chapterId, page: page},
		}).done(function(data){
			if(data.status === "success"){
				$.notify("Successfully deleted that image.", "success");
				loadPages();
			}else{
				$.notify("Something went wrong.", "error");
			}
		});
	});

	$("body").on('change', ".actualUploadBtn", function(){
		$(this).closest("form").submit();
	});

	$('body').on("submit", ".pageForm", function(e){
		e.preventDefault();
		if(!$(this).find(".actualUploadBtn").val()) return;

		var image = $(this).siblings('.pageView');
		$.ajax({
			url: "/acp/chapter/page/edit",
			type: 'POST',

			// Form data
			data: new FormData($(this)[0]),
			cache: false,
			contentType: false,
			processData: false,
			progress: function(e) {
				if(e.lengthComputable) {
					var pct = (e.loaded / e.total) * 100;
					progress.progressbar("value", pct);
				}else {
					console.warn('Content Length not reported!');
				}
			}}).done(function(data){
				if(data.status === "success"){
					$.notify("Successfully uploaded that image.", "success");
					image.removeAttr('src').attr('src', "http://dcmanga.pw" + data.newPage + "?" + new Date().getTime());
					$(this).find(".actualUploadBtn").val("");
					$(".progress").hide();
				}else{
					$.notify("Something went wrong.", "error");
				}
		});

	});

	$("body").on('click', ".add span", function(){
		$(".addFileInput").trigger("click");
	});

	$("body").on('change', ".addFileInput", function(){
		$(this).closest("form").submit();
	});

	$("body").on("submit", ".add form", function(e){
		e.preventDefault();
		if(!$(this).find(".addFileInput").val()) return;

		var image = $(this).siblings('.pageView');

		$.ajax({
			url: "/acp/chapter/page/add",
			type: 'POST',

			// Form data
			data: new FormData($(this)[0]),
			cache: false,
			contentType: false,
			processData: false,
			progress: function(e) {
				if(e.lengthComputable) {
					var pct = (e.loaded / e.total) * 100;
					progress.progressbar("value", pct);
				}else {
					console.warn('Content Length not reported!');
				}
			}}).done(function(data){
			if(data.status === "success"){
				$.notify("Successfully uploaded that image.", "success");
				loadPages();
				$(".progress").hide();
			}else{
				$.notify("Something went wrong.", "error");
			}
		});

	});
});
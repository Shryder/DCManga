$(document).ready(function(){
	$('.comment').each(function(i, obj) {

    	var userid = $(this).attr("userid");
    	var $this = $(this);

    	$.post("/user/" + userid, { id: userid }, function(data, status){
			if(data.status === "success"){
				$this.find(".comment-user").find("a").html(data.username);
			}else{
		    	$this.find(".comment-user").find("a").html("undefined user");
			}
    	});
    });

	$.post("/api/get/chapter", {id: cid}, function(data){
		if(data.status === "success"){
			var pages = [];

			$.each(data.chapter.pages, function(index, current){
				pages.push("/ch/" + cid + "/" + current);
			});
			
			new Reader(pages, "#reader");
		}else if(data.status === "fail"){
			$(".content").remove();
			error(data.message);
		}else{
			error("Something went wrong, please contact an administrator and report this problem.");
		}
	});

	$(".comments-toggle").click(function(){
		$(".comments").toggle();
	});

	$(".comment-toggle").click(function(){
		$(".comments .new-comment").removeAttr("quote");
		$(".comments .new-comment .quote-view").html("");
		$(".comments .new-comment form textarea").val("");
		$(".comments .new-comment form").removeAttr("isEdit");
		$(".comments .new-comment form").removeAttr("editId");
		$(".comments .new-comment").toggle();
		$(".comments .comment-toggle").toggle();
	});

	$(".comments .new-comment .cancel").click(function(){
		$(".comments .new-comment").removeAttr("quote");
		$(".comments .new-comment .quote-view").html("");
		$(".comments .new-comment").hide();
		$(".comments .comment-toggle").show();
	});

	$(".comments .comment .quote").click(function(e){
		e.preventDefault();

		$(".comments .new-comment").attr("quote", $(this).closest(".comment").attr("commentid"));
		$(".comments .new-comment .quote-view").html("Quoting comment #" + $(this).closest(".comment").attr("commentid"));
		$(".comments .new-comment").show();
		$(".comments .comment-toggle").hide();
	});

	$(".comments .comment .comment-actions ul li .edit").click(function(e){
		e.preventDefault();
		$this = $(this);
		$.post("/api/comment/get", {
			commentid: $this.closest(".comment").attr("commentid")
		}, function(data, status){
			$(".comments .new-comment").attr("quote", data.quote);
			if(data.quote){
				$(".quote-view").html("Quoting comment #" + data.quote);
			}else{
				$(".quote-view").html("");				
			}
			$(".comments .new-comment form textarea").val(data.content);
			$(".comments .new-comment form").attr("isEdit", true);
			$(".comments .new-comment form").attr("editId", data.id);
			$(".comments .new-comment").show();
		
		});
	});

	$(".comments .new-comment form").on("submit", function(e){
		var $this = $(this);

		loading(true);
		e.preventDefault();
		var quote = $(".comments .new-comment").attr("quote");
		if (!(typeof quote !== typeof undefined && quote !== false)) {
			quote = null;
		}
		if($(this).attr("isedit") === "true"){
			$.post("/api/comment/edit", {
				commentid: $this.attr('editId'),
				content: $this.find("textarea").val(),
				quote: $(this).closest(".new-comment").attr("quote")
			}, function(data, status){
				loading(false);
				if(data.status === "success"){
					window.local.reload(false);
				}else{
					error("Internal error, please try again later or contact an admin ASAP.");
				}
			});
		}else{
			$.post("/c/" + cid + "/comment", 
			{ 
				content: $(".comments .new-comment form textarea").val(),
				quote: quote
			}, function(data, status){
		        loading(false);
		        if(data.status === "fail"){
		        	error(data.message);
		        }else{
		        	window.location.reload(false);
		        }
		    });
		}
	});
});
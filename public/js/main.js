function error(str){
	if(str === ""){
		$(".error").hide();
		return;
	}
	$('.error').hide();
    $('.error').html(str);
	$('.error').show(300);
}

function success(str){
	if(str === ""){
		$(".success").hide();
		return;
	}
	$('.success').hide();
    $('.success').html(str);
	$('.success').show(300);
}

function loading(bool){
	if($('.loading img').attr('src') === ""){
		$('.loading img').attr('src', "/img/loading.svg");
	}
	if(bool){
		$(".loading").show();
	}else{
		$(".loading").hide();
	}
}

if(mobile){
	$("#right").click(function(e){
		$(".dropdown").toggle();
	});
}else{
	$("#right").hover(function(){
		$(".dropdown").show();
	});
}
$(document).ready(function(){
	$('.register').on("submit", function(e){
		e.preventDefault();
		$(".errors").html("");
		success("");
		error("");
		loading(true);
		
		$.post("/register", $('.register').serialize(), function(data){
	    	loading(false);
	    	console.log(data);
	        if(data.status == "1"){
	        	error("Email already used. Please pick another one.");
	        }else if(data.status == "2"){
	        	error("Username already used. Please pick another one.");
	        }else if(data.status == "3"){
	        	error("Registration failed for some reason, please contact an administrator to solve this problem.");
	        }else if(data.status == "4"){
				success("Successfully registered.");
	        }else if(data.status == "5"){
	        	error("Error");
	        	$('.errors').show();
	      		for (var i = 0; i < data.errors.length; i++) {
					$('.errors').append('<li>' + data.errors[i].msg + '</li>');
	        	}
	        }
	    });
	});

});

var router = require('express').Router()
	, User = require('../Schemas/User.js')
	, Blog = require('../Schemas/Blog.js')
	, multer  = require('multer')
	, fs = require('fs')
	, path = require('path')
	, inArray = require('in-array')
	, mime = require('mime')
	, SUPPORTED = ["image/jpeg", "image/png"]
	, Chapter = require('../Schemas/Chapter.js')
	, randomstring = require("randomstring")
	, unzip = require('unzip')
	, striptags = require('striptags');

function isAdmin(req, res, next){
	if(req.session.user.rank > 1){
		next();
	}else{
		res.sendStatus(404);
	}
}

var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path, function(err){

    });
  }
};

function prettyDate(dateString){
	var date = new Date(dateString);
	var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	return date.getDate()  + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear();
}

router.get('/acp', isAdmin, function(req, res){
	res.render('acp');
});

router.get('/acp/blog', isAdmin, function(req, res){
	res.render('acp/blog');
});

router.get('/acp/blogs', isAdmin, function(req, res){
	res.render('acp/blogs');
});

router.get('/acp/blog/new', isAdmin, function(req, res){
	res.render('acp/blog');
});

router.post('/acp/blog/new', isAdmin, function(req, res){
	new Blog({
		title: req.body.title,
		content: striptags(req.body.content, ['a']),
		date: prettyDate(Date.now()),
		likes: []
	}).save(function(err, data){
		if(err) {
			res.json({status: "fail"});
			throw err;
		}

		res.render("info", {message: "Successfully posted that shit of yours. be gone now thot.<a href='/acp/blog'>Back</a>"});
	});
});

router.post('/acp/blog/edit', isAdmin, function(req, res){
	if(isNaN(req.body.id)){
		res.send("Invalid ID");
		return;
	}
	Blog.findOneAndUpdate({id: req.body.id}, {title: req.body.title, content: striptags(req.body.content, ['a'])}).exec(function(err, data){
		if(err){
			res.json({status: "fail"});
			throw err;
		}else{
			res.render('info', {message: "Sucessfully edited that shit of yours. <a href='/acp/blog/" + req.body.id + "'>Back</a>"})
		}
	});
});

router.post('/acp/blog/delete', isAdmin, function(req, res){
	if(isNaN(req.body.id)){
		res.send("Invalid ID");
		return;
	}

	Blog.findOneAndRemove({id: req.body.id}, function(err, data){
		if(err) throw err;
		res.json({status: "success"});
	});
});

router.get('/acp/blog/:id', isAdmin, function(req, res){
	Blog.findOne({id: req.params.id}, function(err, data){
		if(err) throw err;
		if(data){
			res.render('acp/blog', {blog: data})
		}else{
			res.render("info", {message: "Blog not found."});
		}
	})
});

router.get('/acp/users', isAdmin, function(req, res){
	res.render('acp/users');
});

router.post('/acp/user/edit', isAdmin, function(req, res){

	if(req.body.ban){
		User.findOneAndUpdate({id: req.body.ban}, {banned: true}).exec();
		res.json({status: "success"});
	}else if(req.body.unban){
		User.findOneAndUpdate({id: req.body.unban}, {banned: false}).exec();
		res.json({status: "success"});

	}
});

router.get('/acp/chapters', isAdmin, function(req, res){
	res.render('acp/chapters');
});


router.get('/acp/chapters/:id', isAdmin, function(req, res){
	if(isNaN(req.params.id)){
		res.sendStatus(404);
		return;
	}
	res.render('acp/chapter', {id: req.params.id});
});

router.post('/acp/chapter/page/edit', multer({ dest: 'uploads/' }).single('page'), function(req, res){
	if(!req.session.user.rank > 1){
		res.sendStatus(404);
		return;
	}

	if(!inArray(SUPPORTED, req.file.mimetype)){
		res.json({status: "fail", message: "Only PNG/JPEG files are allowed."});
		return;
	}


	fs.rename(req.file.path, path.join(__dirname, "../public/ch/" + req.body.chapterId + "/" + req.body.pageId), function(err){
		if(err) throw err;
	});

	res.json({status: "success", newPage: "/ch/" + req.body.chapterId + "/" + req.body.pageId});
});

router.post('/acp/chapter/page/delete', isAdmin, function(req, res){
	fs.unlinkSync(path.join(__dirname, "../public/ch/" + req.body.id + "/" + req.body.page));
	
	Chapter.findOneAndUpdate({id: req.body.id}, {$pull : {pages : req.body.page}}, function(err, data){
		if(err) res.json({status: "fail"})

		res.json({status: "success"});
	});
});

router.post('/acp/chapter/page/add', multer({ dest: 'uploads/' }).single('page'), function(req, res){
	if(!req.session.user.rank > 1){
		res.sendStatus(404);
		return;
	}

	if(!inArray(SUPPORTED, req.file.mimetype)){
		res.json({status: "fail", message: "Only PNG/JPEG files are allowed."});
		return;
	}

	Chapter.findOne({id: req.body.chapterId}, function(err, data){
		var name = randomstring.generate() + Date.now() + "." + mime.getExtension(req.file.mimetype);
		
		if (!fs.existsSync(path.join(__dirname, "../public/ch/" + data.id + "/"))){
		    fs.mkdirSync(path.join(__dirname, "../public/ch/" + data.id + "/"));
		}
		
		fs.rename(req.file.path, path.join(__dirname, "../public/ch/" + req.body.chapterId + "/" + name ), function(err){
			if(err) throw err;
		});

		Chapter.findOneAndUpdate({id: req.body.chapterId}, { $push : { pages: name}}, function(err, sdata){
			if(err) throw err;
		})

	});


	res.json({status: "success", newPage: "/ch/" + req.body.chapterId + "/" + req.body.pageId});
});

router.post("/acp/chapters/delete/", isAdmin, function(req, res){
	if(!req.body.id){
		res.json({status: "fail", message: "Please specify chapter ID."});
		return;
	}
	Chapter.findOne({id: req.body.id}).remove().exec(function(err, data){
		if(err) res.json({status: "fail"});
		
		if(data.n == 0){
			res.json({status: "fail", message: "Already deleted that chapter, or something went wrong."});
			return;
		}
		deleteFolderRecursive("public/ch/" + req.body.id);
		res.json({status: "success"});
	});
});
router.get("/acp/chapter/new", isAdmin, function(req, res){
	res.render("acp/newchapter");
});

router.post("/acp/chapter/new/", isAdmin, function(req, res){
	if(!req.body.title){
		res.send("Please enter a title.");
		return;
	}

	new Chapter({
		title: req.body.title,
		date: prettyDate(Date.now()),
		views: 0,
		uploader: req.session.user.username,
		pages: []
	}).save(function(err, data){
		if(err) {
			console.log(err);
			res.send("Something went wrong, fuck you.");	
			return;
		}
		var full = path.join(__dirname, "../public/ch/" + data.id + "/");
		
		if (!fs.existsSync(full)){
		    fs.mkdirSync(full);
		}

		res.redirect("/acp/chapters/" + data.id);
	});
});

router.post("/acp/chapter/order/", isAdmin, function(req, res){
	if(!req.body.newOrder){
		res.json({status: "fail"});
		return;
	}

	Chapter.findOneAndUpdate({id: req.body.id}, {pages: req.body.newOrder}).exec(function(err, data){
		if(err) throw err;
		res.json({status:"success"});
		return;
	});
});

router.post("/acp/chapter/name/", isAdmin, function(req, res){
	if(!req.body.newName || req.body.newName.length < 4){
		res.json({status: "fail"});
		return;
	}

	Chapter.findOneAndUpdate({id: req.body.id}, {title: req.body.newName}).exec(function(err, data){
		if(err) throw err;
		res.json({status:"success"});
		return;
	});
});

router.post('/acp/chapter/bulk', multer({ dest: 'uploads/' }).single('archive'), function(req, res){
	if(!req.session.user.rank > 1){
		res.sendStatus(404);
		return;
	}
	if(!req.body.id){
		res.json({status: false});
	}

	fs.createReadStream(req.file.path)
	.pipe(unzip.Parse())
	.on('entry', function (entry) {
		var fileName = entry.path;
		var type = entry.type; // 'Directory' or 'File' 
		var size = entry.size;
		var mimetype = path.extname(fileName);
		var dir = path.join(__dirname, "../public/ch/" + req.body.id + "/");

		if (mimetype == ".png" || mimetype == ".jpg" || mimetype == ".jpeg") {
			if(type === "File"){
				var name = randomstring.generate() + Date.now() + mimetype;

				entry.pipe(fs.createWriteStream(dir + name));

				Chapter.findOneAndUpdate({id: req.body.id}, { $push : { pages: name }}, function(err, sdata){
					if(err) throw err;
				});
			}
		} else {
			entry.autodrain();
		}
	});
	fs.unlinkSync(req.file.path);
	res.json({status: "success"});
});
module.exports = router;
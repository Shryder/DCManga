var router = require('express').Router()
	, Chapter = require('../Schemas/Chapter')
	, Info = require('../Schemas/Info')
	, Comment = require('../Schemas/Comment')
	, checkAuth = require('../middlewares/middleware.js');

router.get('/c/:id', function(req, res){
	if(isNaN(req.params.id)){
		res.sendStatus(404);
		return;
	}
	
	Chapter.findOneAndUpdate({id: req.params.id}, {$inc: {views: 1}}).exec();

	Info.incTotalViews();

	Comment.find({chapter: req.params.id}).sort({date: -1}).exec(function(err, comments){
		if(err) throw err;
		res.render('chapter', {id: req.params.id, comments: comments});
	});
});

router.post('/c/:id/comment', checkAuth, function(req, res){
	if(isNaN(req.params.id)){
		res.sendStatus(404);
		return;
	}
	if(!(typeof req.body.content !== 'undefined' && req.body.content)){
		res.sendStatus(404);
		return;
	}
	if(req.body.content.length > 5 && !isNaN(req.body.quote)){
		new Comment({
			userid: req.session.user.id,
			content: req.body.content,
			quote: req.body.quote,
			chapter: req.params.id,
			time: new Date()
		}).save(function(err, data){
			if(err){
				res.json({status: "fail", message:"Unknown error, please tell an admin."});
				return;
			}
			res.json({status:"success"});
		});
	}else{
		res.json({status: "fail", message:"Comment has to contain more than 5 characters."});
		return;
	}
});


// Putting it here for now.
router.post('/api/comment/get', checkAuth, function(req, res){


	Comment.findOne({id: req.body.commentid}, function(err, data){
		if(err) throw err;
		if(data){
			res.json(data);
		}else{
			res.json({status: "fail"});
		}
	});

});

router.post('/api/comment/edit', checkAuth, function(req, res){
	var comment = req.body.commentid
		, newContent = req.body.content
		, newQuote = req.body.quote;

	if(!(typeof newContent !== 'undefined' && newContent) || !(typeof comment !== 'undefined' && comment)){
		res.sendStatus(404);
		return;
	}
	Comment.findOne({id: comment}, function(err, data){
		if(data.userid == req.session.user.id){
			if(newContent.length > 5){
				Comment.findOneAndUpdate({id: comment}, {content: newContent, quote: newQuote}).exec(function(err, data){
					if(err) throw err;
					res.json({status:"success"});
				});
			}
		}else{
			res.json({status:"fail"});
		}
	});
});


module.exports = router;
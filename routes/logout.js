var router = require('express').Router()
	, loginsManager = require('../models/loginsManager');

router.get('/logout', function(req, res){
	if(req.session.user == null) {
		res.render('info', {message: "You are not logged in."});
		return;
	}

	delete req.session.user;
	loginsManager.destroy(req.cookies.token);
	res.clearCookie('token');
	delete req.cookies.token;
	
	res.render('info', {message: "Logging out...", refresh: 1});
});

module.exports = router;
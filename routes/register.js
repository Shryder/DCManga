var router = require('express').Router()
var auth = require('../models/auth');
var validator = require('express-validator');

router.get('/register', function (req, res) {
    res.render('register');
});

router.post('/register', function(req, res){

	req.checkBody('username', 'Username field cannot be empty.').notEmpty();
	req.checkBody('username', 'Username must be between 4-15 characters long.').len(4, 15);
	req.checkBody('email', 'The email you entered is invalid, please try again.').isEmail();
	req.checkBody('email', 'Email address must be between 4-100 characters long, please try again.').len(4, 100);
	req.checkBody('password', 'Password must be between 8-100 characters long.').len(8, 100);
	req.checkBody('passwordConfirm', 'Password must be between 8-100 characters long.').len(8, 100);
	req.checkBody('passwordConfirm', 'Passwords do not match, please try again.').equals(req.body.password);
	req.checkBody('username', 'Username can only contain letters, numbers, or underscores.').matches(/^[A-Za-z0-9_-]+$/, 'i');

	var errors = req.validationErrors();

	if(errors){
		res.json({status: 5, errors: errors});
	}else {
		auth.register(req.body.username, req.body.email, req.body.password, function(response){
			switch(response){
				case "used email":
					res.json({status: "1"});
					break;
				case "used username":
					res.json({status: "2"});
					break;
				case "fail":
					res.json({status: "3"});
					break;
				case "success":
					res.json({status: "4"});
					break;
			}
		});
	}
});
module.exports = router;
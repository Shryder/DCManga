var router = require('express').Router()
    , User = require('../Schemas/User')
    , checkAuth = require('../middlewares/middleware.js')
    , bcrypt = require('bcrypt-nodejs')
    , multer = require('multer')
    , SUPPORTED = ["image/jpeg", "image/png"]
    , inArray = require('in-array')
    , mime = require('mime')
    , randomstring = require('randomstring')
	, fs = require('fs')
	, path = require('path')
	, jimp = require('jimp')
	, Logins = require('../Schemas/Logins.js');



router.get('/settings', checkAuth, function (req, res) {
	res.render('settings');
});

router.get('/settings/password', checkAuth, function(req, res){

	res.render('settings', { options: {		
		action: "/settings/password",
		table: true,
		fields: [
			{
				type: 'password',
				name: 'currentPassword',
				label: 'Current password'
			},
			{
				type: 'password',
				name: 'newPassword',
				label: 'New password'
			},
			{
				type: 'password',
				name: 'retypePassword',
				label: 'Re-type password'
			}
		]}});
});

router.post('/settings/password', checkAuth, function(req, res){
	if(req.body.newPassword != req.body.retypePassword){
		res.json({status: "fail", message: "Passwords are not identical."});
		return;
	}

	if(req.body.newPassword.length < 5){
		res.json({status: "fail", message: "Password must be contain than 5 characters."});
		return;
	}

	User.findOne({username: req.session.user.username}, function(err, data){
		if(data){
			if(bcrypt.compareSync(req.body.currentPassword, data.password)){
				User.findOneAndUpdate({username: req.session.user.username}, {password: bcrypt.hashSync(req.body.newPassword)}).exec();
				delete req.session.user;
				res.clearCookie('user');
				res.render('info', {message: "Successfully changed password.<br/>Logging out.", refresh: 1});
				return;
			}
		}else{
			res.json({status: "fail", message: "Wrong password."});
			return;
		}
	})
});

router.get('/settings/email', checkAuth, function(req, res){
	res.render('settings', {options: {
		table: true,
		action: "/settings/email",
		fields: [
			{
				type: 'email',
				name: 'newEmail',
				label: 'New email address'
			}
		]}});
});

router.post('/settings/email', checkAuth, function(req, res){
	req.checkBody('newEmail', 'The email you entered is invalid, please try again.').isEmail();
	if(req.validationErrors()){
		res.render('info', {message: "The email you entered is invalid."});
	}else{
		User.findOneAndUpdate({id: req.session.id}, {email: req.body.email}).exec();
		res.render('info', {message: "Successfully changed email to " + req.body.email + "."});
	}
});


router.get('/settings/avatar', checkAuth, function(req, res){
	res.render('settings', {options: {
		table: false,
		extrafields: [{
			tag: "img",
			attrs: [{
				name: "src",
				value: "/user/avatar/" + req.session.user.id 
			},
			{
				name: "style",
				value: "float:left;margin-right:1em;"
			}]
		}],
		fileupload: true,
		fields: [{
			type: "file",
			name: "avatar",
			label: "",
			class: "none"
		}]
	}});
});

router.post('/settings/avatar',  multer({ dest: 'uploads/' }).single('avatar'), function(req, res){
	if(!req.session.user) {
		res.sendStatus(404);
		return;
	}

	if(!inArray(SUPPORTED, req.file.mimetype)){
		res.render('info', {message: "Only PNG/JPEG files are allowed."});
		return;
	}

	if (req.file.size > (1000 * 1000 * 5)){
		res.render('info', {message: "A maximum of 5 Mbs is allowed."});
		return;
	}

	var fileName = randomstring.generate(5) + "-" + req.session.user.id + ".jpg";
	
	jimp.read(req.file.path, function(err, lenna){
		if(err) throw err;
		lenna.resize(200, 240).quality(60).write(path.join(__dirname, "/../users/images/" + fileName).toString());
		fs.unlinkSync(req.file.path);
	});

	User.findOne({id: req.session.user.id}, function(err, data){
		fs.unlinkSync(path.join(__dirname, "/../users/images/" + data.avatar));
		User.findOneAndUpdate({id: req.session.user.id}, {avatar: fileName}).exec();
	});
	res.render('info', {message: "Successfully changed profile picture."});
});

router.get('/settings/logins', checkAuth, function(req, res){
	Logins.find({uid: req.session.user.id}, function(err, data){
		if(err) throw err;
		res.render('settings', {options: {
			include: true //Can't use {{> variable}}, fuck this gay shit.
		},
		logins: data});
	});
});

router.post('/settings/logins', checkAuth, function(req, res){
	Logins.findOne({token: req.body.token}).remove().exec(function(err, data){
		if(err) throw err;
		res.json({status: "success"});
	});
});


module.exports = router;
var router = require('express').Router()
  , User = require('../Schemas/User')
  , path = require('path')
  , checkAuth = require('../middlewares/middleware.js');

router.get('/user/:id', function(req, res){
	if(isNaN(req.params.id)){
		res.send("Dude, thats not even a valid ID.<br/>Be gone.");
		return;
	}
	User.findOne({id: req.params.id}, function(err, user){
		if(err) throw err;
		if(!user){
			res.render('info', {message: "No user with such ID was found."});
			return;
		}else{
			res.render('user', {userInfo: user});
		}
	});
});

router.get('/user/avatar/:id', function(req, res){
	User.findOne({id: req.params.id}, function(err, user){
		if(!user){
			res.send({status: "fail", message: "User not found."});
		}else{
			res.sendFile(path.join(__dirname + "/../users/images/" + user.avatar).toString());
		}
	});
});

router.get('/profile', checkAuth, function(req, res){
	res.redirect("/user/" + req.session.user.id);
});

router.get('/user', checkAuth, function(req, res){
	res.redirect("/user/" + req.session.user.id);
});

router.post('/user/:id', function(req, res){
	User.findOne({id: req.params.id}, function(err, user){
		if(!user){
			res.status({status: "fail"});
		}else{
			res.json({
				status: "success",
				username: user.username,
				avatar: user.avatar,
				id: user.id
			});
		}
	});
});
module.exports = router;